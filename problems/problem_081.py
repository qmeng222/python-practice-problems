# Write four classes that meet these requirements.
#
# Name:       Animal
#
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
#
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
#
#
# Name:       Dog, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Bark!"
#
#
#
# Name:       Cat, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"

class Animal:
    def __init__(self, num_of_legs, primary_color):
        self.num_of_legs = num_of_legs
        self.primary_color = primary_color

    def describe(self):
        # 在哪个子类中被调用就返回这个子类的名称
        # return self.__class__.__name__ + " has " + str(self.num_of_legs) + " legs and is primarily " + self.primary_color
        return f"{self.__class__.__name__} has {self.num_of_legs} egs and is primarily {self.primary_color}"

class Dog(Animal):
    def speak(self):
        return 'Bark!'

class Cat(Animal):
    def speak(self):
        return 'Miao!'

class Snake(Animal):
    def speak(self):
        return 'Sssssss!'

cat = Cat(4, 'white')
print(cat.describe()) # Cat has 4 legs and is primarily white
print(cat.speak()) # Miao!
