# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

'''
The ord() function returns the number representing the unicode code of a specified character.
Convert back to character with the chr() function.
The chr() function returns the character that represents the specified unicode.
'''

def shift_letters(s):
    res = ''
    for char in s:
        res += chr(ord(char) + 1)
    return(res)

print(shift_letters('ABBA'))
print(shift_letters('import'))
