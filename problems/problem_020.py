# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

import numbers


def has_quorum(attendees_list, members_list):
    return len(attendees_list) >= 0.5 * len(members_list)
