# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    return ''.join([c for i, c in enumerate(s) if c not in s[:i]])

print(remove_duplicate_letters('abdacb'))
print(remove_duplicate_letters('dcbacd'))
print(remove_duplicate_letters('adb db'))
