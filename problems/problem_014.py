# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    ingredients.sort()
    return ingredients == ["eggs", "flour", "oil"]

print(can_make_pasta(["eggs", "water", "oil"])) # False
print(can_make_pasta(["flour", "eggs", "oil"])) # True
print(can_make_pasta(["eggs", "oil", "flour"])) # True
print(can_make_pasta(["eggs", "oil", "oil"])) # False

