# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    if len(values) == 0:
        return None

    return sum(values) / len(values)


print(calculate_average([]))
print(calculate_average([1, 3, 2]))
print(calculate_average([3.3, 4.7, 4, 3, 0]))