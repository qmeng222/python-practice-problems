# Write a class that meets these requirements.
#
# Name:       Receipt
#
# Required state:
#    * tax rate, the percentage tax that should be applied to the total
#
# Behavior:
#    * add_item(item)   # Add a ReceiptItem to the Receipt
#    * get_subtotal()   # Returns the total of all of the receipt items
#    * get_total()      # Multiplies the subtotal by the 1 + tax rate
#
# Example:
#    item = Receipt(.1)
#    item.add_item(ReceiptItem(4, 2.50))
#    item.add_item(ReceiptItem(2, 5.00))
#
#    print(item.get_subtotal())     # Prints 20
#    print(item.get_total())        # Prints 22


# class Receipt
    # method initializer with tax rate
        # self.tax_rate = tax_rate
        # self.items = new empty list

    # method add_item(self, item)
        # append item to self.items list

    # method get_subtotal(self)
        # sum = 0
        # for each item in self.items
            # increase sum by item.get_total()
        # return sum

    # method get_total(self)
        # return self.get_subtotal() * (1 + self.tax_rate)

class ReceiptItem:
    def __init__(self, quantity, price):
        self.quantity = quantity
        self.price = price

    def get_total(self):
        return self.price *  self.quantity


class Receipt:
    def __init__(self, tax_rate):
        self.tax_rate =  tax_rate
        self.items = []  # [4 x 2.5, 2 x 5.0]

    def add_item(self, item):
        return self.items.append(item)

    def get_subtotal(self):  # before tax
        # sum = 0
        # for item in self.items:
        #     sum += item.get_total()  # ReceiptItem.get_total
        # return sum
        return sum(item.get_total() for item in self.items)

    def get_total(self):  # after tax
        return (1 + self.tax_rate) * self.get_subtotal()


receipt = Receipt(.1)
receipt.add_item(ReceiptItem(4, 2.50))
receipt.add_item(ReceiptItem(2, 5.00))

print(receipt.get_subtotal())     # Prints 20
print(receipt.get_total())        # Prints 22
