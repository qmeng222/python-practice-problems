# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    # if len(values) == 0:
    #     return None

    # return max(values)
    return None if len(values) == 0 else max(values)

print(max_in_list([]))
print(max_in_list([6, -9, 8]))
